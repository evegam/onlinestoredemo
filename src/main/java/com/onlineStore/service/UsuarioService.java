package com.onlineStore.service;

import java.util.List;

import com.onlineStore.domain.Usuario;

public interface UsuarioService {

	public List<Usuario> findAll();
	
	Usuario findByUserId(long userId);
	
	Usuario saveOrUpdateUser(Usuario usuario);
	
	void deleteUser(String id);

	Usuario findById(String id);

	public Usuario save(Usuario usuario);


	

}
