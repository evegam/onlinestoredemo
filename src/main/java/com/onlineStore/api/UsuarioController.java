package com.onlineStore.api;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.onlineStore.domain.Usuario;
import com.onlineStore.exception.ModelNotFoundException;
import com.onlineStore.service.UsuarioService;


@RestController
@CrossOrigin
@RequestMapping(value = "/api/public/v1/usuarios")
public class UsuarioController {

	@Autowired
	private UsuarioService usuarioService;

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Usuario> retriveAllUsers() {
		return usuarioService.findAll();
	}
	
	
	
	@GetMapping(value = "/{userId}")
	public Usuario getUserById(@PathVariable("userId") Long userId) {
	    return usuarioService.findByUserId(userId);
	    }
	 
	@DeleteMapping(value = "/{userId}")
	public ResponseEntity<?> deleteUser(@PathVariable Long userId) {
	    usuarioService.deleteUser(usuarioService.findByUserId(userId).getId());
		return new ResponseEntity("Usuario Eliminado", HttpStatus.OK);
	    }
	
	 //metodo para mostrar un usuario x id
	@GetMapping("/{id}")
	public ResponseEntity<Usuario> getById(@PathVariable String id) {

		Usuario usuario = usuarioService.findById(id);

		if (usuario.getId() == null) {
			throw new ModelNotFoundException("Id no encontrado " + id);
		}

		return new ResponseEntity<Usuario>(usuario, HttpStatus.OK);
	}


	
	//metodo para agregar un usuario
	@PostMapping
	public ResponseEntity<Usuario> save(@RequestBody Usuario usuario) {

		Usuario user = usuarioService.save(usuario);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(user.getId())
				.toUri();

		return ResponseEntity.created(location).build();
	}
	
	@PostMapping
    public ResponseEntity<Usuario> saveOrUpdateUser(@RequestBody Usuario usuario) {
       
		Usuario user = usuarioService.saveOrUpdateUser(usuario);
		
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(user.getUserId())
				.toUri();
        
		//return new ResponseEntity("Usuario agregado", HttpStatus.OK);
		
		return ResponseEntity.created(location).build();
    }

}
