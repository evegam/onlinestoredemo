package com.onlineStore.business;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.onlineStore.domain.Usuario;
import com.onlineStore.repository.UsuarioRepository;
import com.onlineStore.service.UsuarioService;

@Service
public class UsuarioServiceImpl implements UsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Override
	public List<Usuario> findAll() {
		return usuarioRepository.findAll();
	}

	@Override
	public Usuario saveOrUpdateUser(Usuario usuario) {
		return usuarioRepository.save(usuario);
		
	}

	@Override
	public Usuario findByUserId(long userId) {
		// TODO Auto-generated method stub
		return usuarioRepository.findByUserId(userId);
	}

	@Override
	public void deleteUser(String id) {
		usuarioRepository.deleteById(id);
		
	}
	
	@Override
	public Usuario findById(String id) {
		Optional<Usuario> usuario = usuarioRepository.findById(id);
		return usuario.isPresent() ? usuario.get() : new Usuario();
	}

	@Override
	public Usuario save(Usuario usuario) {
		// TODO Auto-generated method stub
		return usuarioRepository.save(usuario);
	}

}
