package com.onlineStore.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "usuarios")
public class Usuario {

	@Id
	private String id;
	private Long userId;
	private String nombre;
	private String apellido;
	private String direccion;
	private String curp;

	
	//Mongo Collection
	
//	{
//	    "_id" : ObjectId("5e7bbfb8b65e9917278f5020"),
//	    "nombre" : "Edmundo",
//	    "apellido" : "Carrillo",
//	    "direccion" : "San Angel",
//	    "curp" : "CABE921129RE"
//	}
//	
	
}
