package com.onlineStore.exception;

import java.time.LocalDateTime;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@RestController
public class ErrorResolver extends ResponseEntityExceptionHandler {

	@ExceptionHandler(ModelNotFoundException.class)
	public final ResponseEntity<ExceptionResponse> modelNotFoundExceptionHandler(ModelNotFoundException ex,
			WebRequest request) {

		ExceptionResponse exceptionResponse = ExceptionResponse.builder().timeStamp(LocalDateTime.now())
				.mensaje(ex.getMessage()).detalle(request.getDescription(false)).build();

		return new ResponseEntity<ExceptionResponse>(exceptionResponse, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(Exception.class)
	public final ResponseEntity<ExceptionResponse> masterExceptionHandler() {

		ExceptionResponse exceptionResponse = ExceptionResponse.builder().timeStamp(LocalDateTime.now()).mensaje("")
				.detalle("").build();

		return new ResponseEntity<ExceptionResponse>(exceptionResponse, HttpStatus.INTERNAL_SERVER_ERROR);

	}
	
	@ExceptionHandler(Exception.class)
	public final ResponseEntity<ExceptionResponse> modelNotFoundExceptionHandler1(ModelNotFoundException ex, WebRequest request) {

		ExceptionResponse exceptionResponse = ExceptionResponse.builder().timeStamp(LocalDateTime.now()).mensaje("")
				.detalle("").build();

		return new ResponseEntity<ExceptionResponse>(exceptionResponse, HttpStatus.NOT_FOUND);

	}
	
	

}
